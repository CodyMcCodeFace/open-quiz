module.exports = {
  "DEV": {
    "username": null,
    "password": null,
    "database": null,
    "storage": `${process.env.databasePath}`,
    "dialect": "sqlite"
  },
  "PROD": {
    "username": null,
    "password": null,
    "database": null,
    "storage": `${process.env.databasePath}`,
    "dialect": "sqlite"
  },
};
