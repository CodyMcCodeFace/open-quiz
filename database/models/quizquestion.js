'use strict';
module.exports = (sequelize, DataTypes) => {
  const QuizQuestion = sequelize.define('QuizQuestion', {
    id: { primaryKey: true, type: DataTypes.STRING },
    questionText: DataTypes.STRING,
    sequence: DataTypes.INTEGER,
    isMultipleChoice: DataTypes.BOOLEAN
  }, {});
  QuizQuestion.associate = function(models) {
    // associations can be defined here
  };
  return QuizQuestion;
};