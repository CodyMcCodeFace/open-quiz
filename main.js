/*
**  Nuxt
*/
const http = require('http');
const { Nuxt, Builder } = require('nuxt');
let config = require('./nuxt.config.js');
config.rootDir = __dirname; // for electron-builder
// Init Nuxt.js
const nuxt = new Nuxt(config);
const builder = new Builder(nuxt);
const server = http.createServer(nuxt.render);
// Build only in dev mode
if (config.dev) {
  builder.build().catch(err => {
    console.error(err) // eslint-disable-line no-console
    process.exit(1)
  });
}
// Listen the server
server.listen();
const _NUXT_URL_ = `http://localhost:${server.address().port}`;
console.log(`Nuxt working on ${_NUXT_URL_}`);

/*
** Electron
*/
process.env.NODE_ENV = process.execPath.search('electron-prebuilt') === -1 ? 'PROD' : 'DEV';

let win = null; // Current window
const electron = require('electron');
const path = require('path');

const app = electron.app;

const { setWindowInstance } = require('./api/windowInstance');

// Resolve database path
process.env.databasePath = path.resolve(app.getPath('userData'), 'database.db');

runMigrations()
  .then(() => {
    const api = require('./api/app');
    api.listen(9897);
  });

const newWin = () => {
  win = new electron.BrowserWindow({
    icon: path.join(__dirname, 'static/icon.png'),
  });

  setWindowInstance(win);

  win.maximize();

  win.on('ready-to-show', () => {
    win.show()
  });

  win.on('closed', () => win = null);
  if (config.dev) {
    // Install vue dev tool and open chrome dev tools
    const { default: installExtension, VUEJS_DEVTOOLS } = require('electron-devtools-installer');
    installExtension(VUEJS_DEVTOOLS.id).then(name => {
      console.log(`Added Extension:  ${name}`);
      win.webContents.openDevTools();
    }).catch(err => console.log('An error occurred: ', err))
    // Wait for nuxt to build
    const pollServer = () => {
      http.get(_NUXT_URL_, (res) => {
        if (res.statusCode === 200) { win.loadURL(_NUXT_URL_); } else { setTimeout(pollServer, 300); }
      }).on('error', pollServer);
    }
    pollServer();
  } else { return win.loadURL(_NUXT_URL_); }
}
app.on('ready', newWin);
app.on('window-all-closed', () => app.quit());
app.on('activate', () => win === null && newWin());

function runMigrations() {
  const Umzug = require('umzug');
  const db = require('./database/models/index');

  const umzug = new Umzug({
    storage: 'sequelize',
    storageOptions: {
      sequelize: db.sequelize,
    },
    migrations: {
      params: [
        db.sequelize.getQueryInterface(),
        db.sequelize.constructor,
        function() {
          throw new Error('Migration tried to use old style done callback');
        }
      ],
      path: './database/migrations',
      pattern: /\.js$/
    },
    logging: function() {
      console.log.apply(null, arguments);
    },
  });

  return umzug.up().then((migrations) => {
    console.log(migrations);
    return migrations;
  });
}