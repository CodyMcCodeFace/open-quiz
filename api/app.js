const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const db = require('../database/models/index');
const QuizController = require('./quizController');

const app = express();

app.use(cors());
app.use(bodyParser.json());

const controllers = [];
addController(QuizController);

function addController(ControllerClass) {
  controllers.push(new ControllerClass(app, express.Router(), db));
}

module.exports = app;