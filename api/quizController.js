const json2csv = require('json2csv').parse;
const { dialog } = require('electron');
const { getWindowInstance } = require('./windowInstance');
const fs = require('fs');

class QuizController {
  constructor(app, router, db) {
    this.db = db;

    router.get('/', this.getQuizzes.bind(this));
    router.get('/:id', this.getQuiz.bind(this));
    router.post('/answer', this.getAnswerCsv.bind(this));

    app.use('/quiz', router);
  }

  getQuizzes(_req, res, next) {
    this.db.Quiz.findAll()
      .then((quizzes) => {
        res.json(quizzes);
      }).catch(next);
  }

  getQuiz(req, res, next) {
    this.db.Quiz.findByPk(req.params.id, {
      include: [
        { 
          model: this.db.QuizQuestion,
          as: 'Questions'
        }
      ],
      order: [
        [
          {
            model: this.db.QuizQuestion,
            as: 'Questions'
          },
          'sequence',
        ],
      ],
    }).then((quiz) => {
      console.log('done');
      res.json(quiz);
    }).catch(next);
  }

  getAnswerCsv(req, res) {
    const body = req.body;

    const csv = json2csv(body);

    const path = dialog.showSaveDialog(getWindowInstance(), {
      title: 'Save results',
    });

    console.log(path);

    fs.writeFileSync(path, csv);

    res.status(200).end();
  }
}

module.exports = QuizController;